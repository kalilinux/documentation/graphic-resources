## Graphic Resources

- We recommend [Inkscape](https://inkscape.org/) when handling any `*.svg`
- See our [Press-Pack](https://gitlab.com/kalilinux/documentation/press-pack) for additional content
